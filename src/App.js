import React from 'react';
import logo from './logo.svg';
import './App.css';
import Chat from './Chat.js';

class App extends React.Component {
  state = {
    loading: true
  };

  async componentDidMount() {
    const messages = await fetch("https://api.myjson.com/bins/1hiqin")
    .then(response =>
      response.ok ? response.json() : Promise.reject(Error('Failed to load')))
    .catch(error => {
      throw error;
    });
    this.setState({
      messages: messages,
      loading: false
    });
  }

  render() {
    const { loading } = this.state;

    if (loading) {
      return <img id="loading" src="./circle.gif"/>;
    }

    return (
      <Chat messages={this.state.messages} user="Vasya"/>
    )
  }
}

export default App;
