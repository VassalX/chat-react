import React from 'react';
import MessageList from './MessageList.js';

class Chat extends React.Component {

    constructor(props){
        super(props);

        this.state = {
            messages: this.props.messages
        }

        this.addMessage = this.addMessage.bind(this);
    }

    addMessage(e){
        if(this._inputElement.value !== ""){
            const d = new Date();
            let newMessage = {
                id: Date.now(),
                message: this._inputElement.value,
                created_at: d.toLocaleString(),
                user: this.props.user
            }

            this.setState((prevState) => {
                return {
                    messages: prevState.messages.concat(newMessage)
                };
            });

            this._inputElement.value = "";
        }

        console.log(this.state.messages);

        e.preventDefault();
    }

    render() {
        console.log(this.props.messages);
        console.log(this.props.user);
        return (
            <div id="chat">
                <div id="header">
                    <div className="chat-name">
                        {this.props.chatName}
                    </div>
                    <div className="user-num-wrap">
                        Users:
                        <span id="user-num">{this.props.userNum}</span>
                    </div>
                    <div className="message-num-wrap">
                        Messages:
                        <span id="message-num">{this.props.messageNum}</span>
                    </div>
                </div>
                <MessageList messages={this.state.messages} user={this.props.user} />
                <form className="message-form" onSubmit={this.addMessage}>
                    <input type="text" name="message" ref={(a) => this._inputElement = a} id="message-input" placeholder="Message" />
                    <button type="submit" id="message-submit">Submit</button>
                </form>
            </div>
        )
    }
}

export default Chat;