import React from 'react';
import Message from './Message.js';

class MessageList extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            deleted: []
        }
        this.onDelete = this.onDelete.bind(this);
    }

    onDelete(id) {
        this.setState({ deleted: this.state.deleted.concat([id]) })
    } 

    render() {
        return (
            <div id="message-list">
                {
                    this.props.messages
                        .filter(m => !this.state.deleted.includes(m.id))
                        .map((message, index) => (
                            <Message key={index} message={message} user={this.props.user} onDelete={this.onDelete} />
                        ))
                }
            </div>
        )
    }
}

export default MessageList;