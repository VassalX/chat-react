import React from 'react';

class Message extends React.Component {

    constructor(props){
        super(props);
        this.deleteMessage = this.deleteMessage.bind(this);
    }

    deleteMessage(){
        this.props.onDelete(this.props.message.id);
    }

    render() {
      let className;
      let control;
      let isSelf = this.props.message.user === this.props.user;
      if (isSelf) {
        className = "your";
        control = (<div className="message-control">
          <button className="edit control">Edit</button>
          <button className="delete control" onClick={this.deleteMessage}>Delete</button>
        </div>);
      } else {
        className = "their";
        control = (<div className="message-control">
          <button className="likes-wrap control">
            Likes
            <span className="likes-num">{this.props.likes}</span>
          </button>
        </div>)
      }
      return (
        <div className={"message " + className}>
          {
            !isSelf &&
            <img className="avatar" src={this.props.message.avatar} alt="avatar"></img>
          }
          <div className="text">
            <div><b>{this.props.message.user}</b> {this.props.message.created_at}</div>
            {this.props.message.message}
          </div>
          <div className="message-control">
            {control}
          </div>
        </div>
      );
    }
  }
  export default Message;